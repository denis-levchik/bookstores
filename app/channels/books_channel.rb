class BooksChannel < ApplicationCable::Channel
  def subscribed
    stream_for 'books_channel'
  end

  def unsubscribed
  end
end
