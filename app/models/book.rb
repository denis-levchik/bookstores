class Book < ApplicationRecord
  belongs_to :publisher
  has_many :items
end
