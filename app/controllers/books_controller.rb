class BooksController < ApplicationController
  def index
    books = Book.all
    render json: books, each_serializer: BookSerializer
  end

  def create
    book = Book.new( { author: params[:author], title: params[:title], publisher_id: params[:publisher_id] } )
    if book.save
      BooksChannel.broadcast_to \
        "books_channel", { author: book.author, title: book.title }
      render json: book, serializer: BookSerializer
    end
  end

  def show
    render json: book, serializer: BookSerializer
  end

  private

  def book
    @book ||= Book.find(params[:id])
  end
end
