module Devise
  class LoginsController < ApplicationController
    def create
      user = Users::Authenticate.new.call(params[:login],params[:password])
      session[:current_user_id] = user.id
    end
  end
end
