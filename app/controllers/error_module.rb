module ErrorModule
  extend ActiveSupport::Concern

  included do
    rescue_from 'ActiveRecord::RecordNotFound' do |exception|
      render json: { error: I18n.t(:error_not_found) + exception.model, status: 404 }, status: 404
    end

    rescue_from 'ArgumentError' do |exception|
      render json: { error: exception.message, status: 422 }, status: 422
    end
  end
end
