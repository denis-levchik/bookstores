class ApplicationController < ActionController::API
  include ErrorModule
  include HttpAcceptLanguage::AutoLocale

  before_action :authorized?
  AUTHENTICATE_USER_EXCEPT_CONTROLLERS = ['devise/sign_ups', 'devise/logins','graphql', 'books']

  def current_user
    @current_user ||= User.find_by(id: session[:current_user_id])
  end

  def authorized?
    render json: { error: I18n.t(:error_user_not_authorized), status: 401 }, status: 401 if 
      current_user.nil? && !AUTHENTICATE_USER_EXCEPT_CONTROLLERS.include?(params[:controller])
  end
end
