module Shops
  module Books
    class OrdersController < ApplicationController
      def create
        update_item = Items::Update.new.call(item, params[:books_sold_count].to_i)
        render json: update_item, serializer: ItemSerializer
      end
    
      private
    
      def item
        @item ||= Item.find_by!(book_id: params[:book_id], shop_id: params[:shop_id])
      end
    end
  end
end
