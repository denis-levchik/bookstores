module Publishers
  class SalesReportsController < ApplicationController
    def show
      report = PublisherSalesReport.new.call(publisher)
      render json: report, each_serializer: ShopSerializer, adapter: :json
    end

    private

    def publisher
      @publisher ||= Publisher.find(params[:publisher_id]) 
    end
  end
end
