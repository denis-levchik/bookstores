module Items
  class Update
    def call(item, books_sold_count)
      item.with_lock do
        validate(item, books_sold_count)
        item.increment!(:books_sold_count, books_sold_count)
      end
    end

    private

    def validate(item, books_sold_count)
      raise ArgumentError, I18n.t(:error_negative_books) if books_sold_count <= 0
      raise ArgumentError, I18n.t(:error_books_sold_count) if 
        item.books_received < item.books_sold_count + books_sold_count
    end
  end
end
