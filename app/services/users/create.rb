module Users
  class Create
    EMAIL_VALIDATOR = /^[^@]+@[^@.]+\.[^@]+$/

    def call(name, email, password)
      validate(name, email, password)
      User.create( { name: name, email: email, password: password } )
    end

    private

    def validate(name, email, password)
      validate_name(name)
      validate_email(email)
      validate_password(password)
    end

    def validate_name(name)
      raise ArgumentError, I18n.t(:error_nil_name) if !name || name.strip.empty?
      raise ArgumentError, I18n.t(:error_uniqueness_name) if User.find_by(name: name)
    end

    def validate_email(email)
      raise ArgumentError, I18n.t(:error_nil_email) if !email || email.strip.empty?
      raise ArgumentError, I18n.t(:error_uniqueness_email) if User.find_by(email: email)
      raise ArgumentError, I18n.t(:error_email) if !email.match(EMAIL_VALIDATOR)
    end

    def validate_password(password)
      raise ArgumentError, I18n.t(:error_nil_password) if !password || password.strip.empty?
    end
  end
end
