module Users
  class Authenticate
    def call(login, password)
      user = load_user(login)
      user.authenticate(password) ? user : (raise ArgumentError, I18n.t(:error_authentication))
    end

    private

    def load_user(login)
      User.find_by(email: login).tap do |u|
        raise ArgumentError, I18n.t(:error_authentication) if u.nil?
      end
    end
  end
end
