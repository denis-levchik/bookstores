class PublisherSalesReport

  def call(publisher)
    shops = search_shops(publisher.id)
    ActiveRecord::Associations::Preloader.new.preload(shops, :items, search_items(publisher.id))
    shops
  end

  private

  def search_shops(id)
    Shop.joins(items: :book)
      .select('shops.*, SUM(books_sold_count) as books_sold_count')
      .group(:id)
      .where(books: { publisher_id: id })
      .order('books_sold_count Desc')
  end

  def search_items(id)
    Item.joins(:book)
      .where(books: { publisher_id: id })
      .preload(:book)
  end
end
