function openConnection() {
  return new WebSocket("ws://127.0.0.1:3000/cable")
}

function getBooks() {
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function() {
    if (this.readyState == 4 && this.status == 200){
      let booklist = document.getElementById("booklist");
      let responses = JSON.parse(this.responseText);

      for (var i = 0; i < responses.length; i++) {
        booklist.insertAdjacentHTML("beforeend", html( 
        responses[i].id, 
        responses[i].author, 
        responses[i].title))
      }
    }
  };
  xhttp.open("GET","http://127.0.0.1:3000/books",true);
  xhttp.send();
}

function html(id, author, title){
  return "[" + 
    id + "]: " + 
    author + " " + 
    title + "<br />"
}

getBooks();

const booksWebSocket = openConnection();

booksWebSocket.onopen = () => {
  const subscribeMSG = { "command":"subscribe", "identifier":"{\"channel\":\"BooksChannel\"}"}
  booksWebSocket.send(JSON.stringify(subscribeMSG))
};

booksWebSocket.onmessage = (msg) => {
    msg = JSON.parse(msg.data)['message'];
    if (typeof msg == "object") {
      let book_new = document.getElementById("book_new");
      book_new.insertAdjacentHTML("beforeend", html( 
      "New book",
      msg['author'], 
      msg['title']))
    }
}



