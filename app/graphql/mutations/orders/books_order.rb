module Mutations
  module Orders
    class BooksOrder < ::Mutations::BaseMutation

      argument :shop_id, Integer, required: true
      argument :book_id, Integer, required: true
      argument :books_sold_count, Integer, required: true
      
      type Types::ItemType
 
      def resolve(shop_id:, book_id:, books_sold_count:)
        Items::Update.new.call(item(book_id, shop_id), books_sold_count)
      end

      private

      def item(book_id, shop_id)
        Item.find_by!(book_id: book_id, shop_id: shop_id)
      end
    end
  end
end
