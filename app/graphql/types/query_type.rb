module Types
  class QueryType < Types::BaseObject
    # Add `node(id: ID!) and `nodes(ids: [ID!]!)`
    include GraphQL::Types::Relay::HasNodeField
    include GraphQL::Types::Relay::HasNodesField

    # Add root-level fields here.
    # They will be entry points for queries on your schema.

    # TODO: remove me
    field :shops, [Publishers::SalesReportType], null: false do
      argument :publisher_id, Integer, required: true
    end

    def shops(publisher_id:)
      PublisherSalesReport.new.call(Publisher.find(publisher_id))
    end
  end
end
