module Types
  module Publishers
    class SalesReportType < Types::BaseObject
      field :id, ID, null: false
      field :name, String, null: false
      field :books_sold_count, Integer, null: false
      field :items, [Types::ItemType], null: false
    end
  end
end
