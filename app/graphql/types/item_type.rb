module Types
  class ItemType < Types::BaseObject
    field :id, ID, null: false
    field :title, String, null: false
    field :copies_in_stock, Integer, null: false

    def copies_in_stock
      object.books_received - object.books_sold_count
    end

    def id
      object.book.id
    end

    def title
      object.book.title
    end
  end
end
