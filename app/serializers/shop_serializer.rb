class ShopSerializer < ActiveModel::Serializer
  attributes :id, :name, :books_sold_count

  has_many :items, key: :books_in_stock, serializer: ItemSerializer
end
