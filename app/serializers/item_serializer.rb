class ItemSerializer < ActiveModel::Serializer
  attributes :id, :title, :copies_in_stock

  def id
    object.book.id
  end

  def title
    object.book.title
  end
  
  def copies_in_stock
    (object.books_received - object.books_sold_count)
  end
end
