# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2021_11_19_124633) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "books", force: :cascade do |t|
    t.bigint "publisher_id", null: false
    t.string "author", null: false
    t.string "title", null: false
    t.index ["publisher_id"], name: "index_books_on_publisher_id"
  end

  create_table "items", force: :cascade do |t|
    t.bigint "book_id", null: false
    t.bigint "shop_id", null: false
    t.integer "books_received", default: 0, null: false
    t.integer "books_sold_count", default: 0, null: false
    t.index ["book_id", "shop_id"], name: "index_items_on_book_id_and_shop_id", unique: true
    t.index ["shop_id"], name: "index_items_on_shop_id"
  end

  create_table "publishers", force: :cascade do |t|
    t.string "name", null: false
  end

  create_table "shops", force: :cascade do |t|
    t.string "name", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.string "email"
    t.string "password_digest"
    t.datetime "created_at", precision: 6, null: false
    t.datetime "updated_at", precision: 6, null: false
  end

  add_foreign_key "books", "publishers"
  add_foreign_key "items", "books"
  add_foreign_key "items", "shops"
end
