class CreateBooks < ActiveRecord::Migration[6.1]
  def change
    create_table :books do |t|
      t.references :publisher, null: false, foreign_key: true
      t.string :author, null: false
      t.string :title, null: false
    end
  end
end
