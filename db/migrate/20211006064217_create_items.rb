class CreateItems < ActiveRecord::Migration[6.1]
  def change
    create_table :items do |t|
      t.references :book, null: false, foreign_key: true, index: false
      t.references :shop, null: false, foreign_key: true
      t.integer :books_received, null: false, default: 0
      t.integer :books_sold_count, null: false, default: 0
    end
    add_index(:items, [:book_id, :shop_id], unique: true)
  end
end
