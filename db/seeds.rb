publishers = Publisher.create([
  { name: 'Levchik' },
  { name: 'Labirint' }, 
  { name: 'Amazon' }
])

books = Book.create([
  { title: 'Чайка', author: 'Антон Чехов', publisher: publishers[0] }, 
  { title: 'Тьма', author: 'Джон Маррс', publisher: publishers[0] },
  { title: 'Единственный', author: 'Джон Маррс', publisher: publishers[1] },
  { title: 'Пассажиры', author: 'Джон Маррс', publisher: publishers[1] },
  { title: 'Добрая самаритянка', author: 'Джон Маррс', publisher: publishers[1] },
  { title: 'Чайка', author: 'Антон Чехов', publisher: publishers[2] },
  { title: 'Ревизор', author: 'Николай Гоголь', publisher: publishers[2] },
  { title: 'Тарас Бульба', author: 'Николай Гоголь', publisher: publishers[2] },
  { title: 'Шерлок Холмс', author: 'Артур Конан-Дойль', publisher: publishers[2] },
  { title: 'Эркюль Пуаро', author: 'Агата Кристиы', publisher: publishers[2] }
])

shops = Shop.create([
  { name: 'Читай город' },
  { name: 'Лабиринт' },
  { name: 'ЛитРес' }
])

items = Item.create([
  { book: books[0], shop: shops[0], books_received: 10, books_sold_count: 5 },
  { book: books[1], shop: shops[0], books_received: 10, books_sold_count: 5 },
  { book: books[2], shop: shops[1], books_received: 10, books_sold_count: 7 },
  { book: books[3], shop: shops[1], books_received: 10, books_sold_count: 7 },
  { book: books[5], shop: shops[1], books_received: 10, books_sold_count: 5 },
  { book: books[6], shop: shops[1], books_received: 10, books_sold_count: 5 },
  { book: books[4], shop: shops[2], books_received: 10, books_sold_count: 5 },
  { book: books[7], shop: shops[2], books_received: 10, books_sold_count: 5 },
  { book: books[8], shop: shops[2], books_received: 10, books_sold_count: 5 },
  { book: books[9], shop: shops[2], books_received: 10, books_sold_count: 5 }
])
