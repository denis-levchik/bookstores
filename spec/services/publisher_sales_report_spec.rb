require 'rails_helper'

RSpec.describe PublisherSalesReport do
  describe do
    let(:publisher_1) { create :publisher }
    let(:publisher_2) { create :publisher }

    let(:shop_1) { create :shop }
    let(:shop_2) { create :shop }

    let(:book_1) { create :book, publisher: publisher_1 }
    let(:book_2) { create :book, publisher: publisher_1 }
    let(:book_3) { create :book, publisher: publisher_2 }
    let(:book_4) { create :book, publisher: publisher_2 }

    let!(:item_1) { create :item, shop: shop_1, book: book_1, books_received: 10, books_sold_count: 0 }
    let!(:item_2) { create :item, shop: shop_1, book: book_3, books_received: 10, books_sold_count: 5 }
    let!(:item_3) { create :item, shop: shop_2, book: book_3, books_received: 10, books_sold_count: 0 }
    let!(:item_4) { create :item, shop: shop_2, book: book_4, books_received: 10, books_sold_count: 6 }
    let!(:item_5) { create :item, shop: shop_1, book: book_2, books_received: 10, books_sold_count: 0 }

    subject { described_class.new.(publisher) }

    context 'when a publisher only has books in one store' do
      let(:publisher) { publisher_1 }
      
      it 'returns the shop with all the items and books' do
        expect(subject).to eq([shop_1])
        expect(subject[0].books_sold_count).to eq(0)
        expect(subject[0].items).to eq([item_1, item_5])
        expect(subject[0].items[0].book).to eq(book_1)
        expect(subject[0].items[1].book).to eq(book_2)
      end
    end

    context 'a publisher whose books are only available in a few stores' do
      let(:publisher) { publisher_2 }

      it 'returns stores sorted by decreasing number of books sold, items and books to them' do
        expect(subject).to eq([shop_2, shop_1])
        expect(subject[0].books_sold_count).to eq(6)
        expect(subject[1].books_sold_count).to eq(5)
        expect(subject[0].items).to eq([item_3, item_4])
        expect(subject[1].items).to eq([item_2])
        expect(subject[0].items[0].book).to eq(book_3)
        expect(subject[0].items[1].book).to eq(book_4)
        expect(subject[1].items[0].book).to eq(book_3)
      end
    end
  end
end
