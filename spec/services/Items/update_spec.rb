require 'rails_helper'

RSpec.describe Items::Update do

  let(:publisher) { create :publisher }
  let(:book) { create :book, publisher: publisher }
  let(:shop) { create :shop }
  let(:item) { create :item, shop: shop, book: book, books_received: 25, books_sold_count: 5 }

  subject { described_class.new.(item, books_sold_count) }

  context 'when you can buy a certain number of books' do
    let(:books_sold_count) { 15 }

    it 'returns the updated item' do
      expect(subject.books_sold_count).to eq 20
    end
  end

  context 'when you can\'t buy a certain number of books' do
    let(:books_sold_count) { 21 }
    it 'returns ArgumentError' do
      expect { subject }.to raise_error(ArgumentError, 'This number of books is out of stock.')
    end
  end

  context 'when you want to buy a negative number of books' do
    let(:books_sold_count) { 0 }
    it 'returns ArgumentError' do
      expect { subject }.to raise_error(ArgumentError, 'You can\'t sell less than one book.')
    end
  end
end
