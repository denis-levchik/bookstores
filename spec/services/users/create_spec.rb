require 'rails_helper'

RSpec.describe Users::Create do
  let(:name) { 'Denis' }
  let(:email) { 'denis@gmail.com' }
  let(:password) { 'password' }

  subject { described_class.new.(name, email, password) }

  context 'when all data is valid' do

    it 'returns the created user' do
      expect(subject).to eq User.find_by(name: name)
    end
  end

  context 'when the name is blank' do
    let(:name) { ' ' }

    it 'returns ArgumentError' do
      expect { subject }.to raise_error(ArgumentError, 'A name cannot be empty.')
    end
  end

  context 'when the name is not unique' do
    let(:user) { create :user }
    let(:name) { user.name }

    it 'returns ArgumentError' do
      expect { subject }.to raise_error(ArgumentError, 'A user with this name already exists.')
    end
  end

  context 'when the email is blank' do
    let(:email) { '' }

    it 'returns ArgumentError' do
      expect { subject }.to raise_error(ArgumentError, 'Email cannot be blank.')
    end
  end

  context 'when the email is not unique' do
    let(:user) { create :user }
    let(:email) { user.email }

    it 'returns ArgumentError' do
      expect { subject }.to raise_error(ArgumentError, 'A user is already registered to this email.')
    end
  end

  context 'when the email is not valid' do
    let(:email) { 'denis' }

    it 'returns ArgumentError' do
      expect { subject }.to raise_error(ArgumentError, 'Email not valid')
    end
  end

  context 'when the password is blank' do
    let(:password) { nil }

    it 'returns ArgumentError' do
      expect { subject }.to raise_error(ArgumentError, 'The password cannot be blank.')
    end
  end
end
