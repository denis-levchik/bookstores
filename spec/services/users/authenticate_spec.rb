require 'rails_helper'

RSpec.describe Users::Authenticate do
  let(:user) { create :user }

  subject { described_class.new.(user.email, password) }

  context 'when the password is correct' do
    let(:password) { "password" }

    it 'returns user.id' do
      expect(subject).to eq user
    end
  end

  context 'when the password is incorrect' do
    let(:password) { "passwor" }

    it 'returns ArgumentError' do
      expect { subject }.to raise_error(ArgumentError, 'Authentication failed')
    end
  end
end
