require "rails_helper"

RSpec.describe 'Page displaying all books', :type => :system do
  it 'returns books from the database and updates the page', js: true do
    publisher = Publisher.create({name: 'Labirint'})
    book = Book.create( { author: 'Levchik', title: 'Life', publisher_id: publisher.id } )

    visit 'file:///home/den/bookstores/app/front/channels/index.html'
    post '/books', :params => { author: 'Book', title: 'New', publisher_id: publisher.id }

    expect(page).to have_text('Booklist')
    expect(page).to have_text('[New book]: Book New')
    expect(page).to have_text("[#{book.id}]: Levchik Life")
  end
end
