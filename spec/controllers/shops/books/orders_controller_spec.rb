require 'rails_helper'


RSpec.describe Shops::Books::OrdersController, type: :controller do

  describe '#update' do
    let(:user) {create :user}
    let!(:login_user) { session[:current_user_id] = user.id }
    let(:publisher) { create :publisher }
    let(:shop) { create :shop }
    let(:book) { create :book, publisher: publisher}
    let!(:item) { create :item, shop: shop, book: book, books_received: 25, books_sold_count: 5}

    before { post :create, params }

    context 'when the product is in the database and is added correctly' do
      let(:params) { { params: { 'shop_id' => shop.id, 'book_id' => book.id, 'books_sold_count' => 15 } } }

      it 'returns the updated record and a status 200' do
        expect(response).to match_response_schema('item')
        expect(response.status).to eq 200
      end
    end

    context 'when the product is in the database, but the number of books added is a negative number' do
      let(:params) { { params: { 'shop_id' => shop.id, 'book_id' => book.id, 'books_sold_count' => -15 } } }

      it 'returns JSON with an error result and a 422 status' do
        expect(response).to match_response_schema('error')
        expect(response.status).to eq 422
      end
    end

    context 'when the product is not in the database' do
      let(:params) { { params: { 'shop_id' => (shop.id + 1), 'book_id' => book.id, 'books_sold_count' => 21 } } }

      it 'returns JSON with an error result and a 404 status' do
        expect(response).to match_response_schema('error')
        expect(response.status).to eq 404
      end
    end
  end
end
