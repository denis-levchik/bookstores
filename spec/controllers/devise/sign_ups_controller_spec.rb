require 'rails_helper'

RSpec.describe Devise::SignUpsController, type: :controller do

  describe '#create' do
    let(:name) { "Denis" }
    let(:email) { "denis@email.com" }
    let(:password) { "password" }

    before { post :create, params }

    context "when the correct data came in" do
      let(:params) { { params: { 'name' => name, 'email' => email, 'password' => password } } }

      it "returns the user and status 200" do
        expect(response).to match_response_schema('user')
        expect(response.status).to eq 200
      end
    end

    context "when the incorect data came in" do
      let(:params) { { params: { 'name' => '', 'email' => email, 'password' => password } } }

      it "returns error" do
        expect(response).to match_response_schema('error')
        expect(response.status).to eq 422
      end
    end
  end
end
