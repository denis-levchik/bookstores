require 'rails_helper'

RSpec.describe Devise::LoginsController, type: :controller do

  describe '#create' do
    let(:user) { create :user }

    before { 
      post :create, params 
    }

    context "when you entered the correct email and password" do
      let(:params) { { params: { 'login' => user.email, 'password' => user.password } } }

      it "returns the user and status 204" do
        expect(session[:current_user_id]).to eq(user.id)
        expect(response.status).to eq 204
      end
    end

    context "when the login is incorrect" do
      let(:params) { { params: { 'login' => ' ' } } }

      it "returns error and status 422" do
        expect(response).to match_response_schema('error')
        expect(response.status).to eq 422
      end
    end
  end
end
