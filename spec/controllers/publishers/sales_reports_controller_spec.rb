require 'rails_helper'

RSpec.describe Publishers::SalesReportsController, type: :controller do

  describe '#show' do
    let(:user) {create :user}
    let!(:login_user) { session[:current_user_id] = user.id }
    let(:publisher) { create :publisher }
    let(:shop) { create :shop }
    let(:book) { create :book, publisher: publisher}
    let!(:item) { create :item, shop: shop, book: book, books_received: 10, books_sold_count: 5}

    before { get :show, params }

    context 'when the publisher is in the database' do
      let(:params) { { params: { 'publisher_id' => publisher.id } } }

      it 'returns the correct JSON report and status 200' do
        expect(response).to match_response_schema('publisher_sales_report')
        expect(response.status).to eq 200
      end
    end

    context 'when the publisher is not in the database' do
      let(:params) { { params: { 'publisher_id' => (publisher.id + 1) } } }

      it 'returns JSON with an error result and a 404 status' do
        expect(response).to match_response_schema('error')
        expect(response.status).to eq 404
      end
    end
  end
end
