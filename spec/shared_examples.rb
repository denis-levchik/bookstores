RSpec.shared_examples 'when not in the database' do
  it 'returns JSON with an error result and a 404 status' do
    json = JSON.parse(response.body)
    error = json['errors'][0]
    expect(error).to match_response_schema('error_graphQL')
    expect(error['extensions']).to eq({ 'code' => 'NOT_FOUND' })
  end
end