require 'rails_helper'

RSpec.describe Mutations::Orders::BooksOrder, type: :request do
  describe 'resolve' do
    let(:publisher) { create :publisher }
    let(:shop) { create :shop }
    let(:shop_id) { shop.id }
    let(:book) { create :book, publisher: publisher }
    let(:book_id) { book.id }
    let!(:item) { create :item, shop: shop, book: book, books_received: 25, books_sold_count: 5 }
    let(:query) do
      <<~GQL
        mutation BooksOrder($input: BooksOrderInput!) {
          booksOrder(input: $input) {
            id
            title
            copiesInStock
          }
        }
      GQL
    end
    let(:params) do 
      { 
        params: {
          query: query, 
          variables: variables 
        }
      } 
    end
    before{ post '/graphql', params.merge(as: :json) }

    context 'when the correct data comes in' do
      let(:variables) do
        {
          input: {
            shopId: shop_id,
            bookId: book_id,
            booksSoldCount: 15
          }
        }
      end

      it 'updates and returns item' do
        expect(item.reload).to have_attributes(
          'books_sold_count' => 20
        )
        json = JSON.parse(response.body)
        data = json['data']['booksOrder']
        expect(data).to match_response_schema('item_graphQL')
      end
    end

    context 'when the product is in the database, but the number of books added is a negative number' do
      let(:variables) do
        {
          input: {
            shopId: shop_id,
            bookId: book_id,
            booksSoldCount: -15
          }
        }
      end

      it 'returns error message' do
        json = JSON.parse(response.body)
        error = json['errors'][0]
        expect(error).to match_response_schema('error_graphQL')
        expect(error['extensions']).to eq({ 'code' => 'UNPROCESSABLE_ENTITY' })
      end
    end
    
    context 'when the product is not in the database' do
      let(:variables) do
        {
          input: {
            shopId: 0,
            bookId: 0,
            booksSoldCount: 15
          }
        }
      end

      include_examples 'when not in the database'
    end
  end
end
