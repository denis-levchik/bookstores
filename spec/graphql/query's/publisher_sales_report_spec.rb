require 'rails_helper'


RSpec.describe Types::Publishers::SalesReportType, type: :request do

  describe 'resolve' do
    let(:publisher) { create :publisher }
    let(:publisher_id) { publisher.id }
    let(:shop) { create :shop }
    let(:book) { create :book, publisher: publisher}
    let!(:item) { create :item, shop: shop, book: book, books_received: 10, books_sold_count: 5}
    let(:query) do
      <<~GQL
        query PublisherSalesReport($input: Int!){
          shops (publisherId: $input){
            id,
            name,
            booksSoldCount,
            items{
              id,
              title,
              copiesInStock
            }
          }
        }
      GQL
    end
    let(:params) do 
      { 
        params: {
          query: query, 
          variables: variables 
        }
      } 
    end
    before{ post '/graphql', params.merge(as: :json) }

    context 'when the publisher is in the database' do
      let(:variables) do 
        {
          input: publisher_id
        } 
      end

      it 'returns the correct JSON report' do
        expect(response.body).to match_response_schema('publisher_sales_report_graphQL')
      end
    end

    context 'when the publisher is not in the database' do
      let(:variables) do 
        {
          input: 0
        } 
      end

      include_examples 'when not in the database'
    end
  end
end