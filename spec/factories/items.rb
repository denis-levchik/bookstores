FactoryBot.define do
  factory :item do
    association :book
    association :shop
  end
end
