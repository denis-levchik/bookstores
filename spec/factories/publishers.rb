FactoryBot.define do
  factory :publisher do
    name { FFaker::Company.name }
  end
end
