FactoryBot.define do
  factory :book do
    association :publisher
    author { FFaker::Book.author }
    title { FFaker::Book.title }
  end
end
