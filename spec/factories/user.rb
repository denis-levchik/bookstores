FactoryBot.define do
  factory :user do
    name { FFaker::Book.author }
    email { FFaker::Internet.email }
    password { "password" }
  end
end
