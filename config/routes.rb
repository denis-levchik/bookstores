Rails.application.routes.draw do
  mount ActionCable.server => '/cable'

  post "/graphql", to: "graphql#execute"
  if Rails.env.development?
    mount GraphiQL::Rails::Engine, at: "/graphiql", graphql_path: "/graphql"
  end

  resources :publishers, only: [] do
    resource :sales_report, only: [:show], module: 'publishers'
  end

  resources :shops, only: [] do
    resources :books, module: 'shops', only: [] do
      resource :order, only: [:create], module: 'books'
    end
  end

  resources :books, only: [:show, :index, :create]

  scope module: 'devise' do
    resource :sign_up, :login, only: [:create]
  end
end
